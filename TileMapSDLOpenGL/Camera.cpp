#include "Camera.h"
#include <iostream>
#define GLM_FORCE_RADIANS
#include <glm.hpp> 
#include <gtc/matrix_transform.hpp> 
#include <gtx/transform.hpp>
#include <gtc/type_ptr.hpp> 


Camera::Camera()
{
	
	this->horizontalAngle = 3.14f;
	this->verticalAngle = 0.0f;
	this->mouseSpeed = 0.01f;
	
}

Camera::~Camera()
{
}

void Camera::setCameraPosition( float x, float y, float z)
{
	this->position = glm::vec3(x, y, z);
}
void Camera::setCameraDirection( float x, float y, float z)
{
	this->direction = glm::vec3( x, y, z );
}
void Camera::setCameraUpAxis( float x, float y, float z)
{
	this->upAxis = glm::vec3( x, y, z );
}

void Camera::calculateViewMatrix()
{	
	this->viewMatrix = glm::lookAt( this->position, this->position + this->direction , this->upAxis );
}

void Camera::setCameraFrustrum( float fieldOfview, float aspect, float nearPlane, float farPlane)
{
	this->projectionMatrix = glm::perspective( fieldOfview, aspect, nearPlane, farPlane );
	this->calculateViewMatrix();
}

glm::mat4 Camera::getModelViewProjectionMatrix( glm::mat4 modelMatrix  ){
	return  this->projectionMatrix * this->viewMatrix * modelMatrix;
}

void Camera::moveCamera( float x, float y, float z)
{

	if( x > 0 ) 
	{
		this->position = this->position + this->direction;
	}
	if( x < 0 )
	{
		this->position = this->position - this->direction;
	}
	this->calculateViewMatrix();
}

void Camera::rotateCamera ( float horizontalAmmount, float verticalAmmount )
{
	this->horizontalAngle += horizontalAmmount;
	this->verticalAngle += verticalAmmount;
	
	this->direction = glm::vec3(
			cos(verticalAngle) * sin(horizontalAngle),
			sin(verticalAngle),
			cos(verticalAngle) * cos(horizontalAngle)
		);
		
	glm::vec3 right = glm::vec3(
			sin(horizontalAngle - 3.14f/2.0f),
			0,
			cos(horizontalAngle - 3.14f/2.0f)
		);
		
	this->upAxis = glm::cross( right, direction );

	this->calculateViewMatrix();
			
}
