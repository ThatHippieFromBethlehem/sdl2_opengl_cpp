#ifndef MODEL_LOADER_H
#define MODEL_LOADER_H
#include <GL\glew.h>
#include <gl\gl.h>
#include <string>
#include "Model.h"

class ModelLoader
{
private:

public:
	ModelLoader(void);
	~ModelLoader(void);
	Model* loadModel(std::string filePath);
};

#endif // MODEL_LOADER_H