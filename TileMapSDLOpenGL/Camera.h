#ifndef CAMERA_H
#define CAMERA_H
#define GLM_FORCE_RADIANS
#include <glm.hpp>

class Camera
{
private:
	glm::vec3 position;
	glm::vec3 direction;
	glm::vec3 upAxis;
	glm::mat4 projectionMatrix;
	
	glm::mat4 viewMatrix;
	glm::mat4 modelViewProjectionMatrix;
	glm::mat4 modelMatrix;
	
	
	float mouseSpeed;
	float horizontalAngle;
	float verticalAngle;
	
	void calculateViewMatrix( );
	
public:
	Camera();
	~Camera();

	void rotateCamera( float horizontalAmmount, float verticalAmmount );
	void moveCamera( float x, float y, float z);
	void setCameraPosition( float x, float y, float z);
	void setCameraDirection( float x, float y, float z);
	void setCameraUpAxis( float x, float y, float z);
	void setCameraFrustrum( float fieldOfview, float aspect, float nearPlane, float farPlane);
		
	glm::mat4 getModelViewProjectionMatrix( glm::mat4 modelMatrix  );
	
};

#endif // CAMERA_H
