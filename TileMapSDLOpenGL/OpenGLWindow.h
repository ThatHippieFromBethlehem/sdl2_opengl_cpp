#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <stdio.h>
#include <gl\glew.h>
#include <gl\gl.h>
#include "VisualObject.h"
#include "glm.hpp"
#include "Camera.h"
#include <sdl.h>
#include "InputHandler.h"


class OpenGLWindow : public InputHandler 
{
private:
	// Using SDL2
	SDL_Window * sdlWindow; 
	SDL_GLContext openglContext;	
	const char* windowTitle;
	int windowWidth, windowHeight;
	bool running;	
	
	Camera camera;
	std::vector<VisualObject> objects;
	InputHandler * keyListener;	
	
	void draw();
	
public:
	OpenGLWindow(const char * title, int width, int height);
	~OpenGLWindow();
	// Start handling the messages
	void startLoop();

	bool isRunning();
	void addObject( VisualObject obj );
	void setCamera( Camera myCamera );
	void setListener( InputHandler *keyListener );
	
	// Overloading method from InputHandler
	void keyPressed( SDL_Keycode keycode );
	void mousePressed( int x, int y );
};

#endif // OPENGLWINDOW_H
