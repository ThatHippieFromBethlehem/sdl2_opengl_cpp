#include "Model.h"


Model::Model(void)
{
	this->vertexBufferData = NULL;
	this->vertexBufferSize = 0;
	this->vertexBufferCount = 0;

	this->vertexBufferIndexesData = NULL;
	this->vertexBufferIndexesSize = 0;
	this->vertexBufferIndexesCount = 0;

	this->uvBufferData = NULL;
	this->uvBufferSize = 0;
	this->uvBufferCount = 0;
}


Model::~Model(void)
{
	if( this->vertexBufferData != NULL ) 
	{
		delete ( this->vertexBufferData );
		this->vertexBufferData = NULL;
	}
	if( this->vertexBufferIndexesData != NULL )
	{
		delete ( this->vertexBufferIndexesData );
		this->vertexBufferIndexesData = NULL;
	}
	if( this->uvBufferData != NULL )
	{
		delete ( this->uvBufferData );
		this->uvBufferData = NULL;
	}
}


void Model::setVertexData( GLfloat * bufferData, GLint bufferSize, GLint bufferCount )
{
	this->vertexBufferData = new GLfloat[bufferCount];
	memcpy( this->vertexBufferData, bufferData, bufferSize );
	
	this->vertexBufferSize = bufferSize;
	this->vertexBufferCount = bufferCount;
}

void Model::setVertexIndexData( GLuint * bufferData, GLint bufferSize, GLint bufferCount )
{
	this->vertexBufferIndexesData = new GLuint[bufferSize];
	memcpy( this->vertexBufferIndexesData, bufferData, bufferSize );

	this->vertexBufferIndexesSize = bufferSize;
	this->vertexBufferIndexesCount = bufferCount;
}


void Model::setUvData( GLfloat * bufferData, GLint bufferSize, GLint bufferCount )
{
	this->uvBufferData = new GLfloat[bufferCount];
	memcpy( this->uvBufferData, bufferData, bufferSize );

	this->uvBufferSize = bufferSize;
	this->uvBufferCount = bufferCount;
}

void Model::setNormalData( GLfloat * bufferData, GLint bufferSize, GLint bufferCount )
{
	this->normalBufferData = new GLfloat[bufferCount];
	memcpy( this->normalBufferData, bufferData, bufferSize );
	
	this->normalBufferSize = bufferSize;
	this->normalBufferCount = bufferCount;
}


void Model::getVertexBufferData( VertexData * vertexData ){

	vertexData->vertexBufferData = this->vertexBufferData;
	vertexData->vertexBufferSize = this->vertexBufferSize;
	vertexData->vertexCount = this->vertexBufferCount;

	vertexData->uvBufferData = this->uvBufferData;
	vertexData->uvBufferSize = this->uvBufferSize;
	vertexData->uvCount = this->uvBufferCount;

	vertexData->normalBufferData = this->normalBufferData;
	vertexData->normalBufferSize = this->normalBufferSize;
	vertexData->normalCount = this->normalBufferCount;


	// Indexes
	vertexData->indexBufferData = this->vertexBufferIndexesData;
	vertexData->indexSize = this->vertexBufferIndexesSize;
	vertexData->indexCount = this->vertexBufferIndexesCount;
}