#ifndef SHADER_H
#define SHADER_H

#include <gl\glew.h>
#include <gl\gl.h>

class Shader
{
private:
	const char *vertexShaderName;
	const char *pixelShaderName;
	GLuint shaderId;
	GLuint loadShaders();
	
public:
	Shader(const char * vertexShader, const char * pixelShader );
	~Shader();
	GLuint getShaderId();
};

#endif // SHADER_H
